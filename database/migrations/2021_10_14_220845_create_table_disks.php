<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDisks extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disks', function (Blueprint $table) {
            $table->id();
            $table->string("uuid")->unique();
            $table->integer("size")->default(0)->nullable();
            $table->integer("used")->default(0)->nullable();
            $table->integer("available")->default(0)->nullable();
            $table->string("mountpoint")->nullable();
            $table->string("partition")->nullable();
            $table->string("partition_name")->nullable();
            $table->string("partition_label")->nullable();
            $table->string("disk_state")->nullable();
            $table->integer("plots")->default(0)->nullable();
            $table->integer("failed_plots")->default(0)->nullable();
            $table->tinyInteger("chia_farming_health")->default(0)->nullable();
            $table->timestamps();
        });
        Schema::create('plots', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger("disk_id");
            $table->string("plot_id");
            $table->string("full_path")->nullable();
            $table->string("type")->default('k32')->nullable();
            $table->timestamps();
        });
        Schema::create('disk_farmer', function (Blueprint $table) {
            $table->foreignId('disk_id')->references('id')->on('disks')->onUpdate('cascade');
            $table->foreignId('farmer_id')->references('id')->on('farmers')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disks');
        Schema::dropIfExists('farmer_disk');
    }
}
