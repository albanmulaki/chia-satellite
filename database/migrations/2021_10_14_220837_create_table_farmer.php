<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFarmer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farmers', function (Blueprint $table) {
            $table->id();
            $table->string('hostname');
            $table->string('api_endpoint_uri')->nullable();
            $table->string('server_ip')->nullable();
            $table->string('server_username')->nullable();
            $table->string('farming_plots')->nullable();
            $table->string('farming_size')->nullable();
            $table->string('farming_status')->nullable();
            $table->string('farming_estimtated_network')->nullable();
            $table->string('blockchain_sync_status')->nullable();
            $table->string('current_difficulty')->nullable();
            $table->string('nftplot_points_found_24h')->nullable();
            $table->string('nftplot_points_rate')->nullable();
            $table->string('nftplot_points_balance')->nullable();
            $table->string('nftplot_points_plots')->nullable();
            $table->text('latest_log')->nullable();
            $table->timestamps();
        });
        \App\Models\Farmer::create([
            "hostname"=>"Node 1",
            "api_endpoint_uri"=>"192.168.1.30:4343",
            "server_ip"=>" 192.168.1.30",
            "server_username"=>"amulaki"
        ]);
        \App\Models\Farmer::create([
            "hostname"=>"Node 9",
            "api_endpoint_uri"=>"212.32.250.171:4343",
            "server_ip"=>"212.32.250.171",
            "server_username"=>"amulaki"
        ]);
        \App\Models\Farmer::create([
            "hostname"=>"Node 10",
            "api_endpoint_uri"=>"95.211.59.102:4343",
            "server_ip"=>"95.211.59.102",
            "server_username"=>"amulaki"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farmers');
    }
}
