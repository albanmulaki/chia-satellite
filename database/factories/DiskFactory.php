<?php

namespace Database\Factories;

use App\Models\Disk;
use Illuminate\Database\Eloquent\Factories\Factory;

class DiskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Disk::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $totalSizeGB=$this->faker->numberBetween(1, 16)*1000;
        $size = floor($this->faker->numberBetween(0,$totalSizeGB));
        $usedSize = $size;
        $freeSize= $totalSizeGB - $usedSize;
        $plotted = $usedSize/108.837; //PlotSize static
        return [
            'size'=>$totalSizeGB,
            'used'=>$usedSize,
            'available'=>$freeSize,
            'device'=>"/sdb/sd".$this->faker->randomLetter(),
            'mounted_on'=>"/mnt/A".$this->faker->numberBetween(1,16),
            'label'=>"A".$this->faker->numberBetween(1,16),
            'plots'=>$plotted,
            'failed_plots'=>$this->faker->randomNumber(),
            'health_status'=>1,
        ];
    }
}
