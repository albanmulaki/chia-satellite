<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Farmer;

class FarmerFactory extends Factory
{
    
    
    protected $model = Farmer::class;
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function definition()
    {
        return [
            'hostname'=>$this->faker->domainName(),
            'server_ip'=>$this->faker->ipv4(),
            "server_username" => $this->faker->userName
        ];
    }
}
