<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'App\Http\Controllers\DashboardController@index')->name('dashboard');
Route::get('/farmer', 'App\Http\Controllers\FarmerController@index')->name('farmer');
Route::get('/farmer/{id}', 'App\Http\Controllers\FarmerController@detail')->name('farmer.details');
Route::get('/disk', 'App\Http\Controllers\DashboardController@disk')->name('disk');
