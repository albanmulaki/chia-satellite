<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Farmer extends Model
{
    
    protected $fillable = ['hostname','server_ip','server_username','api_endpoint_uri'];
    
    use HasFactory;
    
    public function disks()
    {
        return $this->belongsToMany(Disk::class);
    }
    
    public function getTiBAsNumber(){
        return (double)$this->farming_size;
    }
}
