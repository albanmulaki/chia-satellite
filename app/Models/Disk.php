<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Disk extends Model
{
    use HasFactory;
    protected $fillable=['size','used','available','mountpoint','device','partition_label','disk_state'];
    
    protected $appends = ['progress_bar_stats','prg'];
    public function farmer()
    {
        return $this->belongsToMany(Farmer::class);
    }
    public function plot()
    {
        return $this->hasMany(Plot::class);
    }
    
    public function getProgressBarStatsAttribute(){
        return [
            "used" => "25%",
            "total" => "100%"
        ];
    }
    public function getPrgAttribute(){
        return $this->attributes['prg']= [
            "used" => "25%",
            "total" => "100%"
        ];
    }
}
