<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FarmerController extends Controller
{

    public function index()
    {
        $farmer = \App\Models\Farmer::with('disks')->get();
        return inertia()->render('Farmer', [
                'farmerList' => $farmer,
        ]);
    }

    public function detail($id)
    {
        $details = \App\Models\Farmer::with(['disks' => function ($disks) {
                        $disks->with('plot')->orderBy('partition_label', 'ASC');
                    }])
                ->where('farmers.id', $id)->first();
	$plotTotalCount = \App\Models\Plot::whereIn('disk_id',$details->disks->pluck('id'))->count();

        return inertia()->render('FarmerDetails', [
                'details' => $details,
                'totalPlots' => $plotTotalCount
        ]);
    }
}
