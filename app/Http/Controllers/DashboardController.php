<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
	private $blockchainList = [
		"XFX"=>"flax",
		"XFL"=>"flora",
		"HDD"=>"hddcoin",
		"XCD"=>"cryptodoge",
		"XNT"=>"skynet",
		"STOR"=>"stor",
		"STAI"=>"stai",
		"AEC"=> "aedge",
		"XVM"=>"venidium",
		"SPARE"=>"spare",
		"XMZ"=>"maize",
		"APPLE"=>"apple"
	];
    public function index()
    {
        $farmers = \App\Models\Farmer::all();
        $plotFarmingSize = "0";
        $plotFarmingCount = 0;
        foreach ($farmers as $farmer) {
            $plotFarmingSize += $farmer->getTiBAsNumber();
            $plotFarmingCount += $farmer->farming_plots;
        }
        $plotFarmingSize .= "TiB";
        $totalXCH = (double) cache()->get('wallet')['-Total Balance'];
        $totalXCHDetail = $totalXCH;
	$totalXCH = number_format($totalXCH,2);
	$otherWallets = [];
//	dd(cache()->get('flax'));
	foreach($this->blockchainList as $currency => $blockchain){
		if(isset(cache()->get($blockchain)['-Total Balance'])){
		$otherWallets[$blockchain]['total'] = (double)cache()->get($blockchain)['-Total Balance'] ;
		$otherWallets[$blockchain]['status'] = cache()->get($blockchain)['Sync status'] ;
		$otherWallets[$blockchain]['currency'] = $currency;
		}
	}

        return inertia()->render('Dashboard', [
                'farmers' => $farmers,
                'plotFarmingSize' => $plotFarmingSize,
                'plotFarmingCount' => $plotFarmingCount,
                'totalXCH'=>$totalXCH,
                'totalXCHDetail'=> $totalXCHDetail,
		"wallet"=>cache()->get('wallet'),
		"otherWallets"=> $otherWallets
        ]);
    }

    public function disk()
    {
        $farmer = \App\Models\Disk::all();
        return inertia()->render('Disk', [
                'farmerList' => $farmer
        ]);
    }
}
