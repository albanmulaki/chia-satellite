<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Disk;
use DB;
use \Illuminate\Support\Facades\Mail;

class MonitorAgent extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:agent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $farmers = \App\Models\Farmer::all();
	DB::table('plots')->truncate();
	$i=0;
	foreach ($farmers as $farmer) {

		try {
                $response = \Illuminate\Support\Facades\Http::get("http://" . $farmer->api_endpoint_uri);
		$responseCollection = $response->collect();
                foreach ($responseCollection['disks']['blockdevices'] as $disk) {
                    if (isset($disk['children'])) {
                        foreach ($disk['children'] as $partition) {
                            if (isset($partition['label']) && strlen($partition['label']) > 1) {
                                if (!$diskModel = Disk::where('uuid', $partition['uuid'])->first()) {
                                    $diskModel = new Disk();
                                }
                                $diskModel->uuid = $partition['uuid'];
                                $diskModel->size = $this->numfmt($partition['fssize']);
                                $diskModel->used = $this->numfmt($partition['fsused']);
                                $diskModel->available = $this->numfmt($partition['fsavail']);
                                $diskModel->mountpoint = $partition['mountpoint'];
                                $diskModel->partition = $partition['path'];
                                $diskModel->partition_label = $partition['label'];
                                $diskModel->partition_name = $partition['label'];
                                $diskModel->disk_state = $disk['state'];
                                $diskModel->save();
                                $diskModel->farmer()->sync([$farmer->id]);
                                if (isset($partition['plots'])) {

                                    foreach ($partition['plots'] as $diskPlot) {
                                        $plot = new \App\Models\Plot();
                                        $plot->disk_id = $diskModel->id;
                                        $plot->plot_id = $this->getHashPlot($diskPlot);
                                        $plot->full_path = $diskPlot;
                                        $plot->save();
                                    }
                                }
                            }
                        }
                    }
                }
                $plotFarmingSize = $farmer->getTiBAsNumber();
dd($responseCollection);
		if ($plotFarmingSize > (double)$responseCollection['chia']['Total size of plots']) {
                    $this->sendMailNotificationDownServices($farmer, $responseCollection);
		}

                $duplicates = DB::table('plots')
                    ->select('plot_id',DB::raw('COUNT(*) as `count`'))
                    ->groupBy('plot_id')
                    ->havingRaw('COUNT(*) > 1')
                    ->get();
                if ($duplicates->count()) {
                    $this->sendMailNotificationDuplicatedPLOT($farmer, $duplicates);
                }
                \App\Models\Farmer::where('id', $farmer->id)->update([
                    "farming_plots" => $responseCollection['chia']['Plot count for all harvesters'] ?? 0,
                    "farming_size" => $responseCollection['chia']['Total size of plots'] ?? 0,
                    "farming_status" => $responseCollection['chia']['Farming status'] ?? 0,
                    "farming_estimtated_network" => $responseCollection['chia']['show']['Estimated network space'] ?? 0,
                    "blockchain_sync_status" => $responseCollection['chia']['show']['Current Blockchain Status'] ?? 0,
                    "latest_log" => $responseCollection['chia']['log'] ?? 0,
                    "nftplot_points_found_24h" => $responseCollection['chia']['wallet']['plots']['Points found (24h)'] ?? 0,
                    "nftplot_points_rate" => $responseCollection['chia']['wallet']['plots']['Percent Successful Points (24h)'] ?? 0,
                    "nftplot_points_balance" => $responseCollection['chia']['wallet']['plots']['Points balance'] ?? 0,
                    "nftplot_points_plots" => $responseCollection['chia']['wallet']['plots']['Number of plots'] ?? 0
                ]);
		if (isset($responseCollection['chia']['wallet']) && isset($responseCollection['chia']['wallet']['-Total Balance']) ) {

                    $wallet = $responseCollection['chia']['wallet'];
                    unset($wallet['plots']);
                    unset($wallet['transactions']);
                    cache()->purge('wallet');
		    cache()->put('wallet', $wallet);
		    $others = $responseCollection;
		    unset($others['disks']);
		    unset($others['chia']);

		    foreach($others as $bcname => $bc){
			    cache()->put($bcname,$bc['wallet']);
		    }

                }
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                $this->info("Farmer ID: " . $farmer->id);
                $this->info("Farmer Details: " . $farmer->toJson());
                $error = "ERROR: Couldnt reach endpoint api";
                $this->error($error);
                $this->sendMailNotification($farmer, $error);
            } catch (\Illuminate\Http\Client\ConnectionException $e) {
                $this->info("Farmer ID: " . $farmer->id);
                $this->info("Farmer Details: " . $farmer->toJson());
                $error = "ERROR: Connection refused - Is Agent Running ?";
                $this->error($error);
                $this->sendMailNotification($farmer, $error);
	    }

        }
        //DISK
        return Command::SUCCESS;
    }

    public function getHashPlot($plotPath)
    {
        $plot = basename($plotPath);
        while (strpos($plot, '-')) {
            $plot = substr($plot, strpos($plot, '-') + 1);
        }
        return basename($plot, ".plot");
    }

    public function numfmt($size)
    {

        return shell_exec("numfmt --from=si " . str_replace(",", ".", $size)) / pow(1000, 3);
    }

    public function sendMailNotificationDownServices($farmer, $responseCollection)
    {


	Mail::send('mail.down-service', ['farmer'=>$farmer,'chiaReport'=>$responseCollection],  function ($message) use ($farmer) {
            $message->to('alban.mulaki@gmail.com')->subject($farmer->hostname . ' - [' . $farmer->api_endpoint_uri . '] not farming');
    	});

    }

    public function sendMailNotificationDuplicatedPLOT($farmer, $duplicatedPLOT)
    {
	Mail::send('mail.duplicated_plot', ['farmer'=>$farmer,'duplicatedPlot'=>$duplicatedPLOT],  function ($message) use ($farmer) {
            $message->to('alban.mulaki@gmail.com')->subject($farmer->hostname . ' - [' . $farmer->api_endpoint_uri . '] Duplicated PLOT');
    	});

    }

    public function sendMailNotification($farmer, $message)
    {
        \Illuminate\Support\Facades\Mail::raw($farmer->hostname . ' - [' . $farmer->api_endpoint_uri . '] not farming \r\n' . $message, function ($message) use ($farmer) {
            $message->to('alban.mulaki@gmail.com')->subject($farmer->hostname . ' - [' . $farmer->api_endpoint_uri . '] not farming');
        });
    }
}
