docker exec -w /home/ubuntu/project/ chia-satellite_web chmod -R 777 ./*
docker exec -w /home/ubuntu/project/ chia-satellite_web chmod -R 777 ./.*
docker exec -w /home/ubuntu/project/ chia-satellite_web cp .env-ci .env
docker exec -w /home/ubuntu/project/ chia-satellite_web composer install
docker exec -w /home/ubuntu/project/ chia-satellite_web chmod -R 777 ./*
docker exec -w /home/ubuntu/project/ chia-satellite_web php artisan migrate
docker exec -w /home/ubuntu/project/ chia-satellite_web php artisan db:seed
docker exec -w /home/ubuntu/project/ chia-satellite_web php artisan permission:refresh
docker exec -w /home/ubuntu/project/ chia-satellite_web php artisan key:generate
